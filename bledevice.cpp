#include "bledevice.h"

BLEDevice::BLEDevice(QObject *parent) : QObject(parent),
    currentDevice(QBluetoothDeviceInfo()),
    controller(0),
    service(0),
    serviceBatt(0)
{
    DiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
    DiscoveryAgent->setLowEnergyDiscoveryTimeout(5000);

    connect(DiscoveryAgent, SIGNAL(deviceDiscovered(const QBluetoothDeviceInfo&)), this, SLOT(addDevice(const QBluetoothDeviceInfo&)));
    connect(DiscoveryAgent, SIGNAL(error(QBluetoothDeviceDiscoveryAgent::Error)), this, SLOT(deviceScanError(QBluetoothDeviceDiscoveryAgent::Error)));
    connect(DiscoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));
}

BLEDevice::~BLEDevice()
{
    delete DiscoveryAgent;
    delete controller;
}

QStringList BLEDevice::deviceListModel()
{
    return m_deviceListModel;
}

void BLEDevice::setDeviceListModel(QStringList deviceListModel)
{
    if (m_deviceListModel == deviceListModel)
        return;

    m_deviceListModel = deviceListModel;
    emit deviceListModelChanged(m_deviceListModel);
}

void BLEDevice::resetDeviceListModel()
{
    m_deviceListModel.clear();
}

void BLEDevice::addDevice(const QBluetoothDeviceInfo & device)
{
    if(device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration){
        qDebug()<<"Discovered Device:" << device.name() << "Address:" << device.address().toString()<< "RSSI: " <<device.rssi()<< "dBm";

        if(!m_foundDevices.contains(device.name(),Qt::CaseSensitive)&&(device.name().contains("HTS",Qt::CaseSensitive)||
                                                                       device.name().contains("SFT",Qt::CaseSensitive))){
            m_foundDevices.append(device.name());

            DeviceInfo *dev =new DeviceInfo(device);
            qlDevices.append(dev);
        }
    }
}

void BLEDevice::scanFinished()
{
    setDeviceListModel(m_foundDevices);
    emit scanningFinished();
}

void BLEDevice::deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    if (error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
        qDebug() << "The Bluetooth adaptor is powered off.";
    else if (error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
        qDebug() << "Writing or reading from the device resulted in an error.";
    else
        qDebug() << "An unknown error has occurred.";
}

void BLEDevice::startScan()
{
    qDeleteAll(qlDevices);
    qlDevices.clear();
    m_foundDevices.clear();
    resetDeviceListModel();
    DiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    qDebug()<< "Searching for BLE devices..." ;
}

void BLEDevice::startConnect(int i)
{
    currentDevice.setDevice(((DeviceInfo*)qlDevices.at(i))->getDevice());
    if (controller) {
        controller->disconnectFromDevice();
        delete controller;
        controller = 0;

    }

    controller = new QLowEnergyController(currentDevice.getDevice(), this);
    controller ->setRemoteAddressType(QLowEnergyController::RandomAddress);

    connect(controller, SIGNAL(serviceDiscovered(QBluetoothUuid)), this, SLOT(serviceDiscovered(QBluetoothUuid)));
    connect(controller, SIGNAL(discoveryFinished()), this, SLOT(serviceScanDone()));
    connect(controller, SIGNAL(error(QLowEnergyController::Error)),  this, SLOT(controllerError(QLowEnergyController::Error)));
    connect(controller, SIGNAL(connected()), this, SLOT(deviceConnected()));
    connect(controller, SIGNAL(disconnected()), this, SLOT(deviceDisconnected()));

    controller->connectToDevice();
}

void BLEDevice::serviceDiscovered(const QBluetoothUuid &gatt)
{
    if(gatt==QBluetoothUuid(QBluetoothUuid::HealthThermometer)) {
        bFoundHTService =true;
        qDebug() << "UART service found";
    }

    if(gatt==QBluetoothUuid(QBluetoothUuid::BatteryService)){
        bFoundBattService=true;
        qDebug()<< "BATTservicefound";
    }
}

void BLEDevice::serviceScanDone()
{
    delete service;
    service=0;

    if(bFoundHTService){
        qDebug()<<"Connectingto HTS service...";
        service=controller->createServiceObject(QBluetoothUuid(QBluetoothUuid::HealthThermometer),this);
    }

    if(!service){
        qDebug()<<"HTSservicenotfound";
        return;
    }
    connect(service, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),this,
            SLOT(serviceStateChanged(QLowEnergyService::ServiceState)));
    connect(service, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),this,
            SLOT(updateData(QLowEnergyCharacteristic,QByteArray)));
    connect(service, SIGNAL(descriptorWritten(QLowEnergyDescriptor,QByteArray)),this,
            SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor,QByteArray)));
    service->discoverDetails();if(bFoundBattService){
        qDebug()<<"Connecting to Battery service...";
        serviceBatt=controller->createServiceObject(QBluetoothUuid(QBluetoothUuid::BatteryService),this);
    }
    if(!serviceBatt){qDebug()<<"Battservicenotfound";
        return;
    }
    connect(serviceBatt,SIGNAL(stateChanged(QLowEnergyService::ServiceState)),this,
            SLOT(serviceBattStateChanged(QLowEnergyService::ServiceState)));
    connect(serviceBatt,SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),this,
            SLOT(updateData(QLowEnergyCharacteristic,QByteArray)));
    connect(serviceBatt, SIGNAL(descriptorWritten(QLowEnergyDescriptor,QByteArray)),this,
            SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor,QByteArray)));
    serviceBatt->discoverDetails();
}

void BLEDevice::deviceDisconnected()
{
    qDebug() << "Remote device disconnected";
    emit connectionEnd();
}

void BLEDevice::deviceConnected()
{
    qDebug() << "Device connected";
    controller->discoverServices();
}

void BLEDevice::controllerError(QLowEnergyController::Error error)
{
    qDebug() << "Controller Error:" << error;
}

void BLEDevice::serviceStateChanged(QLowEnergyService::ServiceState s)
{
    switch(s){
    case QLowEnergyService::ServiceDiscovered:{
        //Temperature Measurement characteristic
        const QLowEnergyCharacteristic TempMeasChar = service->characteristic(QBluetoothUuid(QBluetoothUuid::TemperatureMeasurement));
        if(!TempMeasChar.isValid()){qDebug()<< "Temperature Measurementcharacteristic not found";
            break;
        }//Temperature Measurement indicate enabled
        const QLowEnergyDescriptor m_notificationDescTm =TempMeasChar.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        if( m_notificationDescTm.isValid() ){ service->writeDescriptor(m_notificationDescTm, QByteArray::fromHex("0200"));
            //enableindication}
            qDebug()<<"ConnectionStart";emit connectionStart();
            break;
        }
    }
    default:
        break;
    }
}

void BLEDevice::confirmedDescriptorWrite(const QLowEnergyDescriptor &d, const QByteArray &value)
{
    qDebug() << "Notification enabled:"<<d.name()<<value.toHex();
}
void BLEDevice::serviceBattStateChanged(QLowEnergyService::ServiceState s)
{
    switch(s){
    case QLowEnergyService::ServiceDiscovered:{const QLowEnergyCharacteristic BattChar=serviceBatt->characteristic(QBluetoothUuid(QBluetoothUuid::BatteryLevel));
        if(!BattChar.isValid()){qDebug()<<"Battcharacteristicnotfound";break;}
        //Batterynotificateenabled
        const QLowEnergyDescriptor m_notificationDesc =BattChar.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        if(m_notificationDesc.isValid()){
            serviceBatt->writeDescriptor(m_notificationDesc,QByteArray::fromHex("0100"));
            //enablenotification
        }
        break;
    }
    default:
        break;
    }
}
void BLEDevice::writeData(QByteArray v)
{
    const QLowEnergyCharacteristic  TxChar = service->characteristic(QBluetoothUuid::TemperatureMeasurement);
    service->writeCharacteristic(TxChar, v, QLowEnergyService::WriteWithoutResponse);
}

void BLEDevice::updateData(const QLowEnergyCharacteristic &c, const QByteArray &v)
{
    if(c.uuid()==QBluetoothUuid(QBluetoothUuid::TemperatureMeasurement)){
        return;

        QList<QVariant>data;
        data.clear();
        data.append(1);
        data.append(2);
        emit newData(data);
    }
}
